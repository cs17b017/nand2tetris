@main
0;JMP
//division
(division)
@0
D=A
@q // quotient
M=D
@r  //remainder
M=D
// dividend is b, divisor is a
(loop)
    // if b<a
    @b
    D=M
    @a
    D=D-M
    @remainder
    D;JLT
    // else subtract a from b and 1 to q
    @a
    D=M
    @b
    M=M-D
    @1
    D=A
    @q
    M=M+D

    // jump to loop
    @loop
    0;JMP
(remainder)
    @b
    D=M
    @r
    M=D
    @end
    0;JMP
(end)
    @last_checkpoint
    0;JMP

(main)
@m
D=M
@b
M=D
@division
0;JMP
(last_checkpoint)
@r
D=M
@b
M=D