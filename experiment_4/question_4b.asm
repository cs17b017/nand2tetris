@main
0;JMP
(function)
@zero_condition
D; JLE
@next_condition
D; JGT
(zero_condition)
    @1
    D=A
    @a
    M=0
    @last_checkpoint
    0;JMP
(next_condition)
@a // D will contain the function argument
M=D
@1
D=D-A // Decrease D by 1
@function
    0;JMP
(last_checkpoint)
@ans
M=D

@1 // increment a by 1
D=A
@a
M=M+D
//again a at initial value

@a
D=M
@2
D=D-A
@function
    0;JMP
(last_checkpoint)
@ans
M=M+D

@2 // increment a by 2
D=A
@a
M=M+D
//again a at initial value

@a
D=M
@n
D=D-M
@back_to_main
D;JGE

@ans
D=M
@last_checkpoint
0;JMP

(main)
@n
D=M
@function
0;JMP
(back_to_main)
(end)
    @end
    0;JMP

