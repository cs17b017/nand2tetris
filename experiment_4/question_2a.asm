// odd even
@0
D=A
@q // quotient
M=D
@r  //remainder
M=D
@2
D=A
@a
M=D
// dividend is b, divisor is 2
(loop)
    // if b<a
    @b
    D=M
    @a
    D=D-M
    @remainder
    D;JLT
    // else subtract a from b and 1 to q
    @a
    D=M
    @b
    M=M-D
    @1
    D=A
    @q
    M=M+D

    // jump to loop
    @loop
    0;JMP
(remainder)
    @b
    D=M
    @r
    M=D
    @end
    0;JMP
(end)
    @1
    D=A
    @r
    D=M-D
    @odd
    D;JEQ
    @even
    0;JMP
(odd)
    @1
    D=A
    @ODD
    M=D
    @programEnd
    0;JMP
(even)
    @1
    D=A
    @EVEN
    M=D
    @programEnd
    0;JMP
(programEnd)
    @programEnd
    0;JMP


