// for addition
@a
D=M
@b
M=M+D
// -------------------------------------------------------------------------------------------------------
// for subtraction
@a
D=M
@b
M=M-D
// -------------------------------------------------------------------------------------------------------
// for multiplication
@c // answer will be stored here
M=0
@a
D=M
@i // i=a
M=D
(loop)
    // for checking constraint
    @i
    D=M
    @end
    D:JLE

    // updating value of c
    @b
    D=M
    @c
    M=M+D

    // decreasing i by 1
    @1
    D=-A
    @i
    M=M+D
    
    // loop jump condition
    @loop
    0;JMP
(end)
    @end
    0;JMP
// -------------------------------------------------------------------------------------------------------
//division
@0
D=A
@q // quotient
M=D
@r  //remainder
M=D
// dividend is b, divisor is a
(loop)
    // if b<a
    @b
    D=M
    @a
    D=D-M
    @remainder
    D;JLT
    // else subtract a from b and 1 to q
    @a
    D=M
    @b
    M=M-D
    @1
    D=A
    @q
    M=M+D

    // jump to loop
    @loop
    0;JMP
(remainder)
    @b
    D=M
    @r
    M=D
    @end
    0;JMP
(end)
    @end
    0:JMP
// -------------------------------------------------------------------------------------------------------



