@main
    0;JMP
// for multiplication
(multiplication)
@c // answer will be stored here
M=0
@a
D=M
@i // i=a
M=D
(loop)
    // for checking constraint
    @i
    D=M
    @end
    D;JLE

    // updating value of c
    @b
    D=M
    @c
    M=M+D

    // decreasing i by 1
    @1
    D=-A
    @i
    M=M+D
    
    // loop jump condition
    @loop
    0;JMP
(end)
    @last_checkpoint
    0;JMP
// function for multiplication
(main)
@0
D=A
@i
M=D
@j
M=D
(loop1)
    //check condition
    @i
    D=M
    @n
    D=M-D
    @end_loop1
    D;JGE
(loop2)
    //check condition
    @j
    D=M
    @m
    D=M-D
    @end_loop2
    D;JGE  
    
    @i
    D=M
    @a
    M=D

    @n
    D=M
    @b
    M=D
    @multiplication
    0;JMP
(last_checkpoint)
    @c
    D=M
    @index
    M=D

    // addition part
    @index
    D=M
    @X
    A=A+D
    D=M

    @temp_a
    M=D
    
    @index
    D=M
    @Y
    A=A+D
    D=M

    @temp_b
    M=D

    @temp_a
    M=M+D

    @index
    D=M
    @Z
    D=A+D
    @var
    M=D // this now contains the address of Z[i][j]

    @temp_a
    D=M
    @var
    A=M
    M=D

    //increment condition
    @1
    D=A
    @j
    M=M+D
(end_loop2)
    //increment condition
    @1
    D=A
    @i
    M=M+D
(end_loop1)